1. Sound effects
  * if possible, a small bit of background music
2. Gameplay additions:
  * powerups: change movement type, remove pawn's attack and movement restrictions
  * new units
  * choose difference board sizes
3. Further visual polish
  * animate the piece making the attack, and returning to it's spot

Bugs of note:

 * [low] the possible move squares don't change if you increase speed via the debug menu. work around: deselect and reselect the piece
