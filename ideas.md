# LD50 Theme

Delay the inevitable

opposing forces are overwhelming:

* advancing army, eg. FTL, space invaders, missile defense
* chess-like where the opponent keeps getting new pieces
* extinction is inevitable eg. human consumption and ecological destruction

time is running out:

* push against the impossible: keep the sun from rising or setting, pour fuel into the sun to
  stop it from burning out, ...
* the school day is going to end, but you WANT class to go on forever!
* the work day is going to end but you have a deadline...
* family dinner time is coming, but you just need "one more minute" with your game

a stat is running out:

* tamagotchi with a medical condition: keep managing, while trying to achieve some other goal.
* poisoned, growing weaker
* going insane, surrounded by monsters through a strange land. it's not a question of if, but when.

a physical resource is running out:

* the land cannot produce enough crops
* mineral shortage causing...

an event

* running a race (like a marathon), and your stamina is running out. keep going as far as you can
  * refreshments every X... balancing speed by stamina drain

the difficulty ramps up until it's physically impossible

* eg. tetris

the delay could be undefined, or a temporary reprieve. thus the hero could "succeed", but as the
story is told it is only to hold back the end for a brief moment.

## rough thoughts

The difference between delaying the inevitable and surviving is that in delaying, you can only ever
lose.

I like the chess-like since it gives a clear base line: the game already exists and I don't have to invent arbitrary mechanics, and the resources for the base game are clear. It might be difficult
to make an opponent that makes meaningful moves. There are a lot of possible modifiers.

* ranking up your pieces (move increases, mutatation into difference pieces, abilities)
* the enemy pieces can get new abilities
* new types of pieces can be added
* the game could be played on a slightly different size of board
* the 'score' is how many turns you've survived

In a similar vein, the race running idea seems to be clear and doable. I feel doubtful that I
can do something to make the gameplay of running interesting. It could be something like trying
to keep as steady of a cadence as possible hitting two keys (eg. L, R, L, R). A lot of runner
games have you just "run" on their own and you can slightly slow down or speed up by moving "up
and down" on the screen. I don't want to do that. The score here again is quite clear: distance
travelled before you run out of stamina.

I like the ideas of the the ill tamagotchi and pushing against something truly inevitable,
like the sun rising. What is less clear to me is what I would actually want to the "game" to be
in these cases.

In the case of the ill tamagotchi, as time passes or health worsens more conditions could be piled
on. This may add tasks, sort of like the new things you always have to do in Cook, Serve, Delicious;
wherein interruptions for bathrooms, cleaning, garbage all happen regularly. It could be quick-time
type events where you need to do inputs; or you have to get to a certain (place? person?).

The idea of making class go on longer could be amusing, in a hermione granger sense. Maybe there's
all sorts of tricks to be played. I think the gameplay of something like would be more text/VN
choice game.

Overall, I'm leaning towards the chess-like game at the moment.

# LD50 Ideas

## Delay the inevitable

* advancing army: eg., FTL
* chess where the opponent keeps getting new pieces
  * space invaders
* tamagotchi with a medical condition: keep managing it. while trying to acheive some other goal?

## Fragile

* hang on to something with a precarious control scheme, eg. waiting tables with a stack of plates
* low health (bullet hell, or 1-hit KO)
* navigate social interactions
* perform a task precisely: small errors may have large consequences
  * eg., solder simulator. components crack when overheated, or esd death when touched
  * surgeon sim but more serious? heh

## Make connections

* crap puzzles like missing pipes or wires
* make your own mental connections in solving a puzzle based on clues
  * detective work
* making (and breaking) connections to manage a systemic capacity (eg. MiniMetro)
* social engineering
* connecting pieces with no particular goal: frankenstein monsters, hip bone is connected to the toe bone?
* connect different concepts or systems in a process (eg. that medicine making puzzle game)

## Mutation

* alter yourself to fit better, or to succeed
* the challenge keeps mutating: you need to adapt
* managing with extra functionality due to mutation (eg. your job now has extra tasks that you need to do)
* identifying mutations (spot the difference?)

## Teleportation

* teleportation is complicated. your star trek teleporter takes you through hell before you rematerialize
* you can only teleport, and so have to go back and force to solve a problem
* movement is always in steps instead of linear. eg. you move like a chess piece while everything around you moves freely

## A single resource

* literally any game with just one resource: life, time, etc.
* finding 'creative' uses for the one thing you have

## Folklore

* an experience based on a folk story?
* story building "I heard that..."
  * make combined with some elements of a rock-paper-scissors version of burning wheel's social
  combat to try and find which narrative is accepted as part of the story

## Shelter

* crosses over in part with infestation
* survival
* home maintenance

## Infestation

* burn all the things
* the paranoia and discomfort of living with bedbugs
* spending energy to control or modify a chaotic environment to prevent the infestation from coming in
* where's waldo
* bop 'em reflex thing

## Road trip

* bored again trail
* cross country canada
* management of driving resources
  * attention / awakeness
  * consumables (snacks, water, coffee)
  * activities (games, stories, podcasts/music/etc)
  * need to pee
  * gas / cash / grass
  * passenger irritation
  * weather / heat / where the 
  * does your cruise control work

## Tunnels

* is this like a special case of make connections?

## Overgrown

* is this like infestation?
  * more precise in not wanting to remove something?
* difficult to move or see what's coming
 
## Out of order

* find pieces of a thing
* find a way to return to calm
* enforce the order beat 'em up

## Space is limited

* any game I make basically
* many games progress from small levels to larger. perhaps fixed size, or they progress from larger to smaller and it becomes more difficult (how?)

## Combine

* put items together
* connect different concepts or systems in a process (eg. that medicine making puzzle game)

## Garden

* a large multi-tomagotchi situation
* sorting or optimization problem: given space and plants and their neighbour interactions, get the
  best results possible
* plant identification (lol just flash cards, but in the compo I can't use outside pictures of a thing unless I take them myself or alter them in a meaningful way)
