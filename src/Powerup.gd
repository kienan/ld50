extends Node2D

var target_attribute
var target_value
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func initialize(attribute, value, text):
	self.target_attribute = attribute
	self.target_value = value
	get_node("Control").set_tooltip(text)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
