extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var height
var width
# holds weakrefs to the square nodes
var squares = {}

const TILES = [
	"Body",
	"Body1",
	"Body2",
]

func initialize(width: int = 8, height: int = 8, rng = null):
	rng = null
	# Remove any Squares beneath us
	for i in self.squares.values():
		if i.get_ref():
			i.get_ref().queue_free()

	self.squares = {}
	self.height = height
	self.width = width
	
	# Create new squares
	var i = 0
	while i < self.width:
		var j = 0
		while j < self.height:
			var instance = ResourceLoader.load("res://src/Square.tscn").instance()
			if rng == null:
				instance.get_node("Body").set_visible(true)
			else:
				var n = rng.randi() % TILES.size()
				instance.get_node(TILES[n]).set_visible(true)
			# @TODO any tweaks to the node by calling custom function initialize()
			instance.translate(Vector2(128*i, 128*j))
			self.squares[Vector2(i, j)] = weakref(instance)
			instance.get_node("Control").set_tooltip(str(Vector2(i, j)))
			add_child(instance)
			j += 1
		i += 1

func to_index(row: int, col: int):
	# 0-based basing width, height
	if row < 0 or col < 0:
		return null
	if row >= self.width or col >= self.height:
		return null
	return (row*self.width)+col

# Called when the node enters the scene tree for the first time.
func _ready():
	initialize(8, 8)
	# pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
