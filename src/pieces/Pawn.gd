extends Piece


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.speed = 1

func get_possible_moves(position): # implemented by our children
	# Pawns only move "forward" with respect to their player
	var directions = []
	var is_player = false
	var forward = Vector2(0, 1)
	var attacks = [Vector2(1, 1), Vector2(-1, 1)]
	if self.is_in_group("player"):
		forward = Vector2(0, -1)
		attacks = [Vector2(1, -1), Vector2(-1, -1)]
		is_player = true
	
	var options = []
	#print(self, " at current position: ", position)
	#print("Is player piece: ", is_player, " and is going in direction: ", forward)
	var i = 1;
	while i < self.speed+1:
		var o = self.init_move_dict(Vector2(position.x, position.y + i*forward.y))
		o.attack = false
		options.append(o)
		i += 1
	options = [options]
	if self.at_spawn:
		var o = self.init_move_dict(Vector2(position.x, position.y + (self.speed+1)*forward.y))
		o.attack = false
		options[0].append(o)
	for d in attacks:
		var d_opts = []
		i = 1
		while i < self.speed+1:
			var o = self.init_move_dict(Vector2(position.x + i*d.x, position.y + i*d.y))
			o.attack_only = true
			d_opts.append(o)
			i += 1
		options.append(d_opts)
	return options	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
