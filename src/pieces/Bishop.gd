extends Piece


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_possible_moves(position):
	var directions = [
		Vector2(1, 1),
		Vector2(1, -1),
		Vector2(-1, -1),
		Vector2(-1, 1),
	]
	var options = []
	for d in directions:
		var d_opts = []
		var i = 1
		while i < self.speed + 1:
			var opt = self.init_move_dict(
				Vector2(position.x+(d.x*i), position.y+(d.y)*i)
			)
			d_opts.append(opt)
			i += 1
		options.append(d_opts)
	return options
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
