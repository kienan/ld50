extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal stat_change_requested

var last_piece = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_piece_info(piece):
	self.last_piece = piece
	get_node("Vbox/Damage/Label").set_text(str(piece.damage))
	get_node("Vbox/Movement/Label").set_text(str(piece.speed))
	get_node("Vbox/Health/Label").set_text(str(piece.health))
	get_node("Vbox/Kills/Label").set_text(str(piece.kills))
	if piece.jump:
		get_node("Vbox/Jump").set_text("Can jump")
		get_node("Vbox/CheckButton").set_pressed(true)
	else:
		get_node("Vbox/Jump").set_text("Cannot jump")
		get_node("Vbox/CheckButton").set_pressed(false)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_UpMovement_pressed():
	emit_signal("stat_change_requested", self.last_piece, "speed", self.last_piece.speed + 1)


func _on_UpHealth_pressed():
	emit_signal("stat_change_requested", self.last_piece, "health", self.last_piece.health + 1)


func _on_UpDamage_pressed():
	emit_signal("stat_change_requested", self.last_piece, "damage", self.last_piece.damage + 1)


func _on_CheckButton_toggled(button_pressed):
	emit_signal("stat_change_requested", self.last_piece, "jump", button_pressed)
