shader_type canvas_item;

uniform float width:  hint_range(0.0, 50.0);
uniform vec4 color: hint_color;

void fragment() {
	vec2 size = TEXTURE_PIXEL_SIZE * width;
		float outline = texture(TEXTURE, UV + vec2(-size.x, 0)).a;
	outline += texture(TEXTURE, UV + vec2(0, size.y)).a;
	outline += texture(TEXTURE, UV + vec2(size.x, 0)).a;
	outline += texture(TEXTURE, UV + vec2(0, -size.y)).a;
	outline += texture(TEXTURE, UV + vec2(-size.x, size.y)).a;
	outline += texture(TEXTURE, UV + vec2(size.x, size.y)).a;
	outline += texture(TEXTURE, UV + vec2(-size.x, -size.y)).a;
	outline += texture(TEXTURE, UV + vec2(size.x, -size.y)).a;
	outline = min(outline, 1.0);
	
	vec4 ncolor = texture(TEXTURE, UV);
	COLOR = mix(ncolor, color, outline - ncolor.a);
	
}