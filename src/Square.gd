extends Node2D
class_name Square

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var reinforcement = null
var timer = 0
const FLASH_FREQUENCY = 2 # seconds
const FLASH_MIN = Color(1, 0.75, 0.75, 1)
const FLASH_MAX = Color(1, 0.5, 0.5, 1)
const DEFAULT_BODY = Color(1, 1, 1, 1)
# Called when the node enters the scene tree for the first time.

func _ready():
	pass # Replace with function body.

func set_reinforcement(reinforcement):
	self.reinforcement = reinforcement
	self.timer = 0
	print("Set reinforcement on ", self, " : ", reinforcement)
	if reinforcement == null:
		get_node("Body").set_modulate(DEFAULT_BODY)
		get_node("Control").set_tooltip("")
	else:
		get_node("Control").set_tooltip("Arriving unit: " + reinforcement)

func _process(delta):
	if self.reinforcement != null:
		self.timer += delta
		if self.timer >= FLASH_FREQUENCY*2:
			self.timer = 0
		if self.timer >= FLASH_FREQUENCY:
			# Going from dark to light
			get_node("Body").set_modulate(
				lerp(FLASH_MIN, FLASH_MAX, 1-((self.timer-FLASH_FREQUENCY)/FLASH_FREQUENCY))
			)
		else:
			# Going from light to dark
			get_node("Body").set_modulate(
				lerp(FLASH_MIN, FLASH_MAX, self.timer / FLASH_FREQUENCY)
			)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
