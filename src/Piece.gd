extends Area2D
class_name Piece

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal hold_start(piece, event)
signal hold_stop(piece, event)
signal click(piece, event)

# Each piece should belong to a group - player or opponent
#
var health = 1
var damage = 1
var speed = 7
var jump = false
var abilities = []
var kills = 0
var at_spawn = true
const CLICK_THRESHOLD = 0.15 # seconds
var last_click = null
var hold_started = false

var status_timer_max = 0
var status_timer = 0
var status_color_start = Color(1, 1, 1, 1)
var status_color_end = Color(1, 1, 1, 0)

func set_status(message, time, c1: Color = Color(1, 1, 1, 1), c2: Color = Color(1, 1, 1, 0)):
	self.status_timer = time
	self.status_timer_max = time
	self.status_color_start = c1
	self.status_color_end = c2
	get_node("StatusChange").set_modulate(c1)
	get_node("StatusChange").set_text(message)
	get_node("StatusChange").set_visible(true)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func get_possible_moves(position): # implemented by our children
	return []

func _input(event):
	if event is InputEventMouseButton and event.button_index == 1:
		var pos = self.to_local(event.position)
		if (pos.x <= 64.0 and pos.x >= -64.0 and pos.y <= 64.0 and pos.y >= -64.0):
			# inside our collision area
			if event.pressed:
				if self.last_click == null:
					self.last_click = 0
					get_tree().set_input_as_handled()
			else:
				if self.last_click != null and self.last_click <= CLICK_THRESHOLD:
					#print("Click: ", self, event)
					emit_signal("click", self, event)
					# Work-around bug where only the last signal is connected
					# to the Game
					#var game = get_tree().get_root().get_node("/root/Game")
					#if game:
					#	print("Self: ", self)
					#	game._on_piece_click(self, event)
					self.last_click = null
					get_tree().set_input_as_handled()
		if not event.pressed and self.hold_started:
			emit_signal("hold_stop", self, event)
			#print("Hold stop", self, " ", event)
			self.cancel_hold()
			get_tree().set_input_as_handled()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if last_click != null:
		self.last_click += delta
		if not self.hold_started and self.last_click >= self.CLICK_THRESHOLD:
			self.hold_started = true;
			#print("Hold start", self, " ", null)
			emit_signal("hold_start", self, null)
	if self.hold_started:
		self.set_global_position(get_viewport().get_mouse_position())
	if self.status_timer > 0:
		self.status_timer -= delta
		if self.status_timer <= 0:
			get_node("StatusChange").set_visible(false)
		get_node("StatusChange").set_modulate(
			lerp(self.status_color_end, self.status_color_start, self.status_timer / self.status_timer_max)
		)
func cancel_hold():
	self.last_click = null
	self.hold_started = false

func init_move_dict(position):
	var x = {
		"attack": true,
		"pos": position,
		"attack_only": false,
		"jump": self.jump,
		"source": self,
	}
	return x
