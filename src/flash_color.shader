shader_type canvas_item;

uniform vec4 min_color: hint_color;
uniform vec4 max_color: hint_color;
uniform vec4 offset_color: hint_color;
uniform float frequency : hint_range(0.0, 2.0) = 0.5;
uniform bool enable: hint_bool = false;

void fragment() {
	if (enable) {
        vec4 sin_anim = vec4(
			min_color.r * sin(2.0*3.14*frequency*TIME),
			min_color.g * sin(2.0*3.14*frequency*TIME),
			min_color.b * sin(2.0*3.14*frequency*TIME),
			1.0
		);
		vec4 cos_anim = vec4(
			max_color.r * cos(1.0*3.14*frequency*TIME),
			max_color.g * cos(1.0*3.14*frequency*TIME),
			max_color.b * cos(1.0*3.14*frequency*TIME),
			1.0
		);
		vec4 color = texture(TEXTURE, UV);
		color = mix(color, sin_anim, color.a);
		COLOR = mix(color, cos_anim, color.a);
	}
	else {
		COLOR = texture(TEXTURE, UV);
	}
}