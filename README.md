# Échec x Chess

You can only hold the the opponent at bay. But, for how long?

Échec x Chess is a variation on chess where you can (probably) never win. The pieces
may be upgraded over the course of the game, and not all the moves are the exact same
as in chess.

This is a Compo submission for the Ludum Dare 50 with the theme "Delay the inevitable".

The submission may be viewed at https://ldjam.com/events/ludum-dare/50/echec-chess

The game may also be played online or downloaded from https://viashimo.itch.io/echec-x-chess
